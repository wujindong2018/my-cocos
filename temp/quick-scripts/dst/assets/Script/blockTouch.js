
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/blockTouch.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '603fanDJX5Mt54Sxi47Vp/Q', 'blockTouch');
// Script/blockTouch.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BlockTouch = /** @class */ (function (_super) {
    __extends(BlockTouch, _super);
    function BlockTouch() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        //全局属性及类型
        _this.spf_block = [];
        _this.text = 'hello';
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    BlockTouch.prototype.onLoad = function () {
    };
    BlockTouch.prototype.init = function (_possStart) {
        this.pos_start = _possStart; //起始位置
        this.isTouch = false; //是否点中
        //随机改变元素块颜色
        this.i_blockType = Math.floor(Math.random() * this.spf_block.length);
        var children = this.node.children;
        for (var i = 0; i < children.length; i++) {
            if (children[i].name == '1') {
                children[i].getComponent(cc.Sprite).spriteFrame = this.spf_block[this.i_blockType];
            }
        }
    };
    //获取世界坐标
    BlockTouch.prototype.getPosWord = function () {
        var pos_world = cc.v2(0, 0);
        var children = this.node.children;
        for (var i = 0; i < children.length; i++) {
            if (children[i].name == '1') {
                pos_world = this.node.convertToNodeSpaceAR(children[i].getPosition());
                break;
                // children[i].getComponent(cc.Sprite).spriteFrame = this.spf_block[this.i_blockType]
            }
        }
        return pos_world;
    };
    //复位回到起始位置
    BlockTouch.prototype.fuWei = function () {
        this.node.scale = 0.8;
        this.isTouch = false; //是否点中
        this.node.setPosition(this.pos_start);
    };
    //点中了放大
    BlockTouch.prototype.touch = function () {
        this.isTouch = true;
        this.node.scale = 1;
    };
    BlockTouch.prototype.start = function () {
    };
    __decorate([
        property(cc.Label)
    ], BlockTouch.prototype, "label", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], BlockTouch.prototype, "spf_block", void 0);
    __decorate([
        property
    ], BlockTouch.prototype, "text", void 0);
    BlockTouch = __decorate([
        ccclass
    ], BlockTouch);
    return BlockTouch;
}(cc.Component));
exports.default = BlockTouch;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0XFxibG9ja1RvdWNoLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsNEVBQTRFO0FBQzVFLG1CQUFtQjtBQUNuQixzRkFBc0Y7QUFDdEYsOEJBQThCO0FBQzlCLHNGQUFzRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWhGLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXdDLDhCQUFZO0lBQXBEO1FBQUEscUVBc0VDO1FBbkVHLFdBQUssR0FBYSxJQUFJLENBQUM7UUFHdkIsQUFDQSxTQURTO1FBQ1QsZUFBUyxHQUEwQixFQUFFLENBQUM7UUFHdEMsVUFBSSxHQUFXLE9BQU8sQ0FBQzs7UUEyRHZCLGlCQUFpQjtJQUNyQixDQUFDO0lBdkRHLHdCQUF3QjtJQUV4QiwyQkFBTSxHQUFOO0lBR0EsQ0FBQztJQUVELHlCQUFJLEdBQUosVUFBSyxVQUFlO1FBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDLENBQUEsTUFBTTtRQUNsQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFBLE1BQU07UUFDM0IsV0FBVztRQUNYLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyRSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNsQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN0QyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksR0FBRyxFQUFFO2dCQUN6QixRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7YUFDckY7U0FDSjtJQUVMLENBQUM7SUFFRCxRQUFRO0lBQ1IsK0JBQVUsR0FBVjtRQUNJLElBQUksU0FBUyxHQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3hCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2xDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3RDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxHQUFHLEVBQUU7Z0JBQ3pCLFNBQVMsR0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFBO2dCQUNuRSxNQUFLO2dCQUNMLHFGQUFxRjthQUN4RjtTQUNKO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQUVELFVBQVU7SUFDViwwQkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUMsR0FBRyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUEsTUFBTTtRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7SUFFekMsQ0FBQztJQUVELE9BQU87SUFDUCwwQkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBQyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFBO0lBRXJCLENBQUM7SUFFRCwwQkFBSyxHQUFMO0lBRUEsQ0FBQztJQWhFRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzZDQUNJO0lBSXZCO1FBRkMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lEQUVXO0lBR3RDO1FBREMsUUFBUTs0Q0FDYztJQVZOLFVBQVU7UUFEOUIsT0FBTztPQUNhLFVBQVUsQ0FzRTlCO0lBQUQsaUJBQUM7Q0F0RUQsQUFzRUMsQ0F0RXVDLEVBQUUsQ0FBQyxTQUFTLEdBc0VuRDtrQkF0RW9CLFVBQVUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJsb2NrVG91Y2ggZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGxhYmVsOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoW2NjLlNwcml0ZUZyYW1lXSlcbiAgICAvL+WFqOWxgOWxnuaAp+WPiuexu+Wei1xuICAgIHNwZl9ibG9jazogW2NjLlNwcml0ZUZyYW1lXSA9IDxhbnk+W107XG5cbiAgICBAcHJvcGVydHlcbiAgICB0ZXh0OiBzdHJpbmcgPSAnaGVsbG8nO1xuICAgIGlfYmxvY2tUeXBlOiBudW1iZXI7XG4gICAgaXNUb3VjaDogYm9vbGVhblxuICAgIHBvc19zdGFydDogYW55O1xuXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICBvbkxvYWQoKSB7XG5cblxuICAgIH1cblxuICAgIGluaXQoX3Bvc3NTdGFydDogYW55KXtcbiAgICAgICAgdGhpcy5wb3Nfc3RhcnQgPSBfcG9zc1N0YXJ0Oy8v6LW35aeL5L2N572uXG4gICAgICAgIHRoaXMuaXNUb3VjaCA9IGZhbHNlOy8v5piv5ZCm54K55LitXG4gICAgICAgIC8v6ZqP5py65pS55Y+Y5YWD57Sg5Z2X6aKc6ImyXG4gICAgICAgIHRoaXMuaV9ibG9ja1R5cGUgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiB0aGlzLnNwZl9ibG9jay5sZW5ndGgpO1xuICAgICAgICBsZXQgY2hpbGRyZW4gPSB0aGlzLm5vZGUuY2hpbGRyZW47XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChjaGlsZHJlbltpXS5uYW1lID09ICcxJykge1xuICAgICAgICAgICAgICAgIGNoaWxkcmVuW2ldLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gdGhpcy5zcGZfYmxvY2tbdGhpcy5pX2Jsb2NrVHlwZV1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgfVxuXG4gICAgLy/ojrflj5bkuJbnlYzlnZDmoIdcbiAgICBnZXRQb3NXb3JkKCl7XG4gICAgICAgIGxldCBwb3Nfd29ybGQ9Y2MudjIoMCwwKVxuICAgICAgICBsZXQgY2hpbGRyZW4gPSB0aGlzLm5vZGUuY2hpbGRyZW47XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChjaGlsZHJlbltpXS5uYW1lID09ICcxJykge1xuICAgICAgICAgICAgICAgIHBvc193b3JsZD10aGlzLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoY2hpbGRyZW5baV0uZ2V0UG9zaXRpb24oKSlcbiAgICAgICAgICAgICAgICBicmVha1xuICAgICAgICAgICAgICAgIC8vIGNoaWxkcmVuW2ldLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gdGhpcy5zcGZfYmxvY2tbdGhpcy5pX2Jsb2NrVHlwZV1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcG9zX3dvcmxkO1xuICAgIH1cblxuICAgIC8v5aSN5L2N5Zue5Yiw6LW35aeL5L2N572uXG4gICAgZnVXZWkoKSB7XG4gICAgICAgIHRoaXMubm9kZS5zY2FsZT0wLjg7XG4gICAgICAgIHRoaXMuaXNUb3VjaCA9IGZhbHNlOy8v5piv5ZCm54K55LitXG4gICAgICAgIHRoaXMubm9kZS5zZXRQb3NpdGlvbih0aGlzLnBvc19zdGFydClcblxuICAgIH1cblxuICAgIC8v54K55Lit5LqG5pS+5aSnXG4gICAgdG91Y2goKXtcbiAgICAgICAgdGhpcy5pc1RvdWNoPXRydWU7XG4gICAgICAgIHRoaXMubm9kZS5zY2FsZT0xXG5cbiAgICB9XG5cbiAgICBzdGFydCgpIHtcblxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=