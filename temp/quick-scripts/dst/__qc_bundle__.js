
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/Script/Game');
require('./assets/Script/block');
require('./assets/Script/blockTouch');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/blockTouch.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '603fanDJX5Mt54Sxi47Vp/Q', 'blockTouch');
// Script/blockTouch.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BlockTouch = /** @class */ (function (_super) {
    __extends(BlockTouch, _super);
    function BlockTouch() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        //全局属性及类型
        _this.spf_block = [];
        _this.text = 'hello';
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    BlockTouch.prototype.onLoad = function () {
    };
    BlockTouch.prototype.init = function (_possStart) {
        this.pos_start = _possStart; //起始位置
        this.isTouch = false; //是否点中
        //随机改变元素块颜色
        this.i_blockType = Math.floor(Math.random() * this.spf_block.length);
        var children = this.node.children;
        for (var i = 0; i < children.length; i++) {
            if (children[i].name == '1') {
                children[i].getComponent(cc.Sprite).spriteFrame = this.spf_block[this.i_blockType];
            }
        }
    };
    //获取世界坐标
    BlockTouch.prototype.getPosWord = function () {
        var pos_world = cc.v2(0, 0);
        var children = this.node.children;
        for (var i = 0; i < children.length; i++) {
            if (children[i].name == '1') {
                pos_world = this.node.convertToNodeSpaceAR(children[i].getPosition());
                break;
                // children[i].getComponent(cc.Sprite).spriteFrame = this.spf_block[this.i_blockType]
            }
        }
        return pos_world;
    };
    //复位回到起始位置
    BlockTouch.prototype.fuWei = function () {
        this.node.scale = 0.8;
        this.isTouch = false; //是否点中
        this.node.setPosition(this.pos_start);
    };
    //点中了放大
    BlockTouch.prototype.touch = function () {
        this.isTouch = true;
        this.node.scale = 1;
    };
    BlockTouch.prototype.start = function () {
    };
    __decorate([
        property(cc.Label)
    ], BlockTouch.prototype, "label", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], BlockTouch.prototype, "spf_block", void 0);
    __decorate([
        property
    ], BlockTouch.prototype, "text", void 0);
    BlockTouch = __decorate([
        ccclass
    ], BlockTouch);
    return BlockTouch;
}(cc.Component));
exports.default = BlockTouch;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0XFxibG9ja1RvdWNoLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsNEVBQTRFO0FBQzVFLG1CQUFtQjtBQUNuQixzRkFBc0Y7QUFDdEYsOEJBQThCO0FBQzlCLHNGQUFzRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWhGLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXdDLDhCQUFZO0lBQXBEO1FBQUEscUVBc0VDO1FBbkVHLFdBQUssR0FBYSxJQUFJLENBQUM7UUFHdkIsQUFDQSxTQURTO1FBQ1QsZUFBUyxHQUEwQixFQUFFLENBQUM7UUFHdEMsVUFBSSxHQUFXLE9BQU8sQ0FBQzs7UUEyRHZCLGlCQUFpQjtJQUNyQixDQUFDO0lBdkRHLHdCQUF3QjtJQUV4QiwyQkFBTSxHQUFOO0lBR0EsQ0FBQztJQUVELHlCQUFJLEdBQUosVUFBSyxVQUFlO1FBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDLENBQUEsTUFBTTtRQUNsQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFBLE1BQU07UUFDM0IsV0FBVztRQUNYLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyRSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNsQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN0QyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksR0FBRyxFQUFFO2dCQUN6QixRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7YUFDckY7U0FDSjtJQUVMLENBQUM7SUFFRCxRQUFRO0lBQ1IsK0JBQVUsR0FBVjtRQUNJLElBQUksU0FBUyxHQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3hCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2xDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3RDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxHQUFHLEVBQUU7Z0JBQ3pCLFNBQVMsR0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFBO2dCQUNuRSxNQUFLO2dCQUNMLHFGQUFxRjthQUN4RjtTQUNKO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQUVELFVBQVU7SUFDViwwQkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUMsR0FBRyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUEsTUFBTTtRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7SUFFekMsQ0FBQztJQUVELE9BQU87SUFDUCwwQkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBQyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFBO0lBRXJCLENBQUM7SUFFRCwwQkFBSyxHQUFMO0lBRUEsQ0FBQztJQWhFRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzZDQUNJO0lBSXZCO1FBRkMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lEQUVXO0lBR3RDO1FBREMsUUFBUTs0Q0FDYztJQVZOLFVBQVU7UUFEOUIsT0FBTztPQUNhLFVBQVUsQ0FzRTlCO0lBQUQsaUJBQUM7Q0F0RUQsQUFzRUMsQ0F0RXVDLEVBQUUsQ0FBQyxTQUFTLEdBc0VuRDtrQkF0RW9CLFVBQVUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJsb2NrVG91Y2ggZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGxhYmVsOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoW2NjLlNwcml0ZUZyYW1lXSlcbiAgICAvL+WFqOWxgOWxnuaAp+WPiuexu+Wei1xuICAgIHNwZl9ibG9jazogW2NjLlNwcml0ZUZyYW1lXSA9IDxhbnk+W107XG5cbiAgICBAcHJvcGVydHlcbiAgICB0ZXh0OiBzdHJpbmcgPSAnaGVsbG8nO1xuICAgIGlfYmxvY2tUeXBlOiBudW1iZXI7XG4gICAgaXNUb3VjaDogYm9vbGVhblxuICAgIHBvc19zdGFydDogYW55O1xuXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICBvbkxvYWQoKSB7XG5cblxuICAgIH1cblxuICAgIGluaXQoX3Bvc3NTdGFydDogYW55KXtcbiAgICAgICAgdGhpcy5wb3Nfc3RhcnQgPSBfcG9zc1N0YXJ0Oy8v6LW35aeL5L2N572uXG4gICAgICAgIHRoaXMuaXNUb3VjaCA9IGZhbHNlOy8v5piv5ZCm54K55LitXG4gICAgICAgIC8v6ZqP5py65pS55Y+Y5YWD57Sg5Z2X6aKc6ImyXG4gICAgICAgIHRoaXMuaV9ibG9ja1R5cGUgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiB0aGlzLnNwZl9ibG9jay5sZW5ndGgpO1xuICAgICAgICBsZXQgY2hpbGRyZW4gPSB0aGlzLm5vZGUuY2hpbGRyZW47XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChjaGlsZHJlbltpXS5uYW1lID09ICcxJykge1xuICAgICAgICAgICAgICAgIGNoaWxkcmVuW2ldLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gdGhpcy5zcGZfYmxvY2tbdGhpcy5pX2Jsb2NrVHlwZV1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgfVxuXG4gICAgLy/ojrflj5bkuJbnlYzlnZDmoIdcbiAgICBnZXRQb3NXb3JkKCl7XG4gICAgICAgIGxldCBwb3Nfd29ybGQ9Y2MudjIoMCwwKVxuICAgICAgICBsZXQgY2hpbGRyZW4gPSB0aGlzLm5vZGUuY2hpbGRyZW47XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChjaGlsZHJlbltpXS5uYW1lID09ICcxJykge1xuICAgICAgICAgICAgICAgIHBvc193b3JsZD10aGlzLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoY2hpbGRyZW5baV0uZ2V0UG9zaXRpb24oKSlcbiAgICAgICAgICAgICAgICBicmVha1xuICAgICAgICAgICAgICAgIC8vIGNoaWxkcmVuW2ldLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gdGhpcy5zcGZfYmxvY2tbdGhpcy5pX2Jsb2NrVHlwZV1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcG9zX3dvcmxkO1xuICAgIH1cblxuICAgIC8v5aSN5L2N5Zue5Yiw6LW35aeL5L2N572uXG4gICAgZnVXZWkoKSB7XG4gICAgICAgIHRoaXMubm9kZS5zY2FsZT0wLjg7XG4gICAgICAgIHRoaXMuaXNUb3VjaCA9IGZhbHNlOy8v5piv5ZCm54K55LitXG4gICAgICAgIHRoaXMubm9kZS5zZXRQb3NpdGlvbih0aGlzLnBvc19zdGFydClcblxuICAgIH1cblxuICAgIC8v54K55Lit5LqG5pS+5aSnXG4gICAgdG91Y2goKXtcbiAgICAgICAgdGhpcy5pc1RvdWNoPXRydWU7XG4gICAgICAgIHRoaXMubm9kZS5zY2FsZT0xXG5cbiAgICB9XG5cbiAgICBzdGFydCgpIHtcblxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/block.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '052a9M5LwBHHIbdKqjByM1J', 'block');
// Script/block.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Block = /** @class */ (function (_super) {
    __extends(Block, _super);
    function Block() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Block.prototype.start = function () {
    };
    Block.prototype.init = function (_iType) {
        this.i_type = _iType;
        this.showBlock();
    };
    //显示哪个色块
    Block.prototype.showBlock = function () {
        var children = this.node.children;
        for (var i = 0; i < children.length; i++) {
            if (children[i].name == this.i_type) {
                children[i].active = true; //显示色块
            }
            else {
                children[i].active = false; //隐藏色块
            }
        }
    };
    __decorate([
        property(cc.Label)
    ], Block.prototype, "label", void 0);
    __decorate([
        property
    ], Block.prototype, "i_type", void 0);
    Block = __decorate([
        ccclass
    ], Block);
    return Block;
}(cc.Component));
exports.default = Block;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0XFxibG9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLDRFQUE0RTtBQUM1RSxtQkFBbUI7QUFDbkIsc0ZBQXNGO0FBQ3RGLDhCQUE4QjtBQUM5QixzRkFBc0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVoRixJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFtQyx5QkFBWTtJQUEvQztRQUFBLHFFQWlDQztRQTlCRyxXQUFLLEdBQWEsSUFBSSxDQUFDOztRQTZCdkIsaUJBQWlCO0lBQ3JCLENBQUM7SUF6Qkcsd0JBQXdCO0lBRXhCLGVBQWU7SUFFZixxQkFBSyxHQUFMO0lBRUEsQ0FBQztJQUVELG9CQUFJLEdBQUosVUFBSyxNQUFZO1FBQ2IsSUFBSSxDQUFDLE1BQU0sR0FBQyxNQUFNLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFDRCxRQUFRO0lBQ1IseUJBQVMsR0FBVDtRQUNJLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2xDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3RDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNqQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFBLE1BQU07YUFDbkM7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQSxNQUFNO2FBQ3BDO1NBQ0o7SUFDTCxDQUFDO0lBM0JEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7d0NBQ0k7SUFHdkI7UUFEQyxRQUFRO3lDQUNDO0lBTk8sS0FBSztRQUR6QixPQUFPO09BQ2EsS0FBSyxDQWlDekI7SUFBRCxZQUFDO0NBakNELEFBaUNDLENBakNrQyxFQUFFLENBQUMsU0FBUyxHQWlDOUM7a0JBakNvQixLQUFLIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci8yLjQvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci8yLjQvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci8yLjQvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCbG9jayBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgbGFiZWw6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eVxuICAgIGlfdHlwZTphbnlcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCgpIHtcblxuICAgIH1cblxuICAgIGluaXQoX2lUeXBlOiB2b2lkKXtcbiAgICAgICAgdGhpcy5pX3R5cGU9X2lUeXBlO1xuICAgICAgICB0aGlzLnNob3dCbG9jaygpO1xuICAgIH1cbiAgICAvL+aYvuekuuWTquS4quiJsuWdl1xuICAgIHNob3dCbG9jaygpIHtcbiAgICAgICAgbGV0IGNoaWxkcmVuID0gdGhpcy5ub2RlLmNoaWxkcmVuO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoY2hpbGRyZW5baV0ubmFtZSA9PSB0aGlzLmlfdHlwZSkge1xuICAgICAgICAgICAgICAgIGNoaWxkcmVuW2ldLmFjdGl2ZSA9IHRydWU7Ly/mmL7npLroibLlnZdcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY2hpbGRyZW5baV0uYWN0aXZlID0gZmFsc2U7Ly/pmpDol4/oibLlnZdcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Game.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '373494UlHRFD5yXVQr/OoZa', 'Game');
// Script/Game.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Game = /** @class */ (function (_super) {
    __extends(Game, _super);
    function Game() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        _this.pre_block_bg = null; //预制资源
        _this.pre_block = null; //预制资源
        _this.pre_layout_block_1 = null; //预制资源
        _this.layout_bg = null;
        _this.layout_block = null;
        _this.text = 'hello';
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    Game.prototype.onLoad = function () {
        var _a, _b;
        this.bg_3 = (_b = (_a = this.node) === null || _a === void 0 ? void 0 : _a.getChildByName('bg')) === null || _b === void 0 ? void 0 : _b.getChildByName("bg_3");
        this.addBlockBg();
        this.addBlock();
        this.addBlockTouch();
        this.setTouch();
    };
    //判断可点击元素块和正常原生块之间的距离
    Game.prototype.pdJuLi_block = function () {
        var _a;
        var pos_touchBlock = cc.v2(0, 0);
        var children_touch = (_a = this.bg_3) === null || _a === void 0 ? void 0 : _a.children;
        for (var i = 0; i < children_touch.length; i++) {
            var js_blockTouch = children_touch[i].getComponent("blockTouch");
            if (js_blockTouch && js_blockTouch.isTouch) {
                var pos_touchWorld = js_blockTouch.getPosWord();
                pos_touchBlock = this.layout_block.convertToNodeSpaceAR(pos_touchWorld);
                0;
            }
        }
        console.log('xa:' + pos_touchBlock.y);
        var children_block = this.layout_block.children;
        for (var i = 0; i < children_block.length; i++) {
            console.log("1111111111111111");
            var pos_block = children_block[i].getPosition();
            var f_x = pos_block.x - pos_touchBlock.x;
            var f_y = pos_block.y - pos_touchBlock.y;
            var f_jl = Math.sqrt(f_x * f_x + f_y * f_y);
            console.log(f_jl);
            if (f_jl < 40) {
                console.log("我来了");
                children_block[i].opacity = 100;
            }
        }
    };
    //屏幕触摸事件
    Game.prototype.setTouch = function () {
        var _a, _b, _c, _d;
        (_a = this.node) === null || _a === void 0 ? void 0 : _a.on('touchstart', function (event) {
            var _a, _b;
            console.log("touchstart");
            var pos_start = event.getLocation();
            var pos_start_bg3 = (_a = this.bg_3) === null || _a === void 0 ? void 0 : _a.convertToNodeSpaceAR(pos_start);
            var children = (_b = this.bg_3) === null || _b === void 0 ? void 0 : _b.children;
            if (children) {
                for (var i = 0; i < children.length; i++) {
                    var rect_blockTouch = children[i].getBoundingBox();
                    if (rect_blockTouch.contains(pos_start_bg3)) {
                        this.pos_touch_start = cc.v2(children[i].x, children[i].y + 150);
                        children[i].setPosition(this.pos_touch_start);
                        console.log("点中了");
                        var js_blockTouch = children[i].getComponent("blockTouch");
                        if (js_blockTouch) {
                            js_blockTouch.touch();
                        }
                        this.pdJuLi_block();
                    }
                }
                console.log('x:' + pos_start_bg3.x, "y:" + pos_start_bg3.y);
            }
        }, this);
        (_b = this.node) === null || _b === void 0 ? void 0 : _b.on('touchmove', function (event) {
            var _a;
            // console.log("touchmove");
            var children = (_a = this.bg_3) === null || _a === void 0 ? void 0 : _a.children;
            if (!children)
                return;
            for (var i = 0; i < children.length; i++) {
                var js_blockTouch = children[i].getComponent("blockTouch");
                if (js_blockTouch && js_blockTouch.isTouch) {
                    // let pos_move = event.getLocation()
                    var pos_move = event.getDelta();
                    this.pos_touch_start.x = this.pos_touch_start.x + pos_move.x;
                    this.pos_touch_start.y = this.pos_touch_start.y + pos_move.y;
                    children[i].setPosition(this.pos_touch_start);
                    console.log('x:', pos_move.x, "y:", pos_move.y);
                    this.pdJuLi_block();
                    // let pos_move_bg3 = this.bg_3?.convertToNodeSpaceAR(pos_move)
                    // children[i].setPosition(pos_move_bg3)
                }
            }
        }, this);
        (_c = this.node) === null || _c === void 0 ? void 0 : _c.on('touchend', function (event) {
            var _a;
            console.log("touchend");
            var children = (_a = this.bg_3) === null || _a === void 0 ? void 0 : _a.children;
            if (!children)
                return;
            for (var i = 0; i < children.length; i++) {
                var js_blockTouch = children[i].getComponent("blockTouch");
                if (js_blockTouch && js_blockTouch.isTouch) {
                    js_blockTouch.fuWei();
                }
            }
        }, this);
        (_d = this.node) === null || _d === void 0 ? void 0 : _d.on('touchcancel', function () {
            var children = this.bg_3.children;
            if (!children)
                return;
            for (var i = 0; i < children.length; i++) {
                var js_blockTouch = children[i].getComponent("blockTouch");
                if (js_blockTouch && js_blockTouch.isTouch) {
                    js_blockTouch.fuWei();
                }
            }
        }, this);
    };
    //添加可以点击的元素块
    Game.prototype.addBlockTouch = function () {
        for (var i = 0; i < 3; i++) {
            var node_layout_block_1 = cc.instantiate(this.pre_layout_block_1);
            node_layout_block_1.parent = this.bg_3;
            var js_blockTouch = node_layout_block_1.getComponent('blockTouch');
            var pos_Blocktouch = cc.v2(0, 14);
            if (i == 0) {
                pos_Blocktouch = cc.v2(-193, 14);
            }
            else if (i == 2) {
                pos_Blocktouch = cc.v2(194, 14);
            }
            node_layout_block_1.setPosition(pos_Blocktouch);
            if (js_blockTouch) {
                js_blockTouch.init(pos_Blocktouch);
            }
        }
    };
    //添加背景块
    Game.prototype.addBlockBg = function () {
        if (this.pre_block_bg == null)
            return;
        for (var i = 0; i < 64; i++) {
            var node_blockBg = cc.instantiate(this.pre_block_bg); //预制体实例化
            node_blockBg.parent = this.layout_bg;
        }
    };
    //添加所有的元素块
    Game.prototype.addBlock = function () {
        if (this.pre_block == null)
            return;
        for (var i = 0; i < 64; i++) {
            var node_block = cc.instantiate(this.pre_block); //预制体实例化
            node_block.parent = this.layout_block;
            node_block.opacity = 0; //所有色块默认透明度为0
            var js_block = node_block.getComponent('block'); //从node_block中获取事件
            if (js_block) {
                console.log("我走到了这里");
                js_block.init(4);
            }
        }
    };
    Game.prototype.start = function () {
    };
    __decorate([
        property(cc.Label)
    ], Game.prototype, "label", void 0);
    __decorate([
        property(cc.Prefab)
    ], Game.prototype, "pre_block_bg", void 0);
    __decorate([
        property(cc.Prefab)
    ], Game.prototype, "pre_block", void 0);
    __decorate([
        property(cc.Prefab)
    ], Game.prototype, "pre_layout_block_1", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "layout_bg", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "layout_block", void 0);
    __decorate([
        property
    ], Game.prototype, "text", void 0);
    Game = __decorate([
        ccclass
    ], Game);
    return Game;
}(cc.Component));
exports.default = Game;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0XFxHYW1lLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsNEVBQTRFO0FBQzVFLG1CQUFtQjtBQUNuQixzRkFBc0Y7QUFDdEYsOEJBQThCO0FBQzlCLHNGQUFzRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWhGLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQWtDLHdCQUFZO0lBQTlDO1FBQUEscUVBMkxDO1FBeExHLFdBQUssR0FBYSxJQUFJLENBQUM7UUFFdkIsa0JBQVksR0FBYyxJQUFJLENBQUMsQ0FBQSxNQUFNO1FBRXJDLGVBQVMsR0FBYyxJQUFJLENBQUMsQ0FBQSxNQUFNO1FBRWxDLHdCQUFrQixHQUFjLElBQUksQ0FBQyxDQUFBLE1BQU07UUFFM0MsZUFBUyxHQUFZLElBQUksQ0FBQztRQUUxQixrQkFBWSxHQUFZLElBQUksQ0FBQztRQUc3QixVQUFJLEdBQVcsT0FBTyxDQUFBOztRQTBLdEIsaUJBQWlCO0lBQ3JCLENBQUM7SUF2S0csd0JBQXdCO0lBRXhCLHFCQUFNLEdBQU47O1FBQ0ksSUFBSSxDQUFDLElBQUksZUFBRyxJQUFJLENBQUMsSUFBSSwwQ0FBRSxjQUFjLENBQUMsSUFBSSwyQ0FBRyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtRQUNmLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtRQUNwQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7SUFDbkIsQ0FBQztJQUVELHFCQUFxQjtJQUNyQiwyQkFBWSxHQUFaOztRQUNJLElBQUksY0FBYyxHQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFBO1FBQzdCLElBQUksY0FBYyxTQUFHLElBQUksQ0FBQyxJQUFJLDBDQUFFLFFBQVEsQ0FBQztRQUN6QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxJQUFJLGFBQWEsR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2pFLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3hDLElBQUksY0FBYyxHQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtnQkFDN0MsY0FBYyxHQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsY0FBYyxDQUFDLENBQUE7Z0JBRXJGLENBQUMsQ0FBQTthQUNZO1NBQ0o7UUFFQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFdEMsSUFBSSxjQUFjLEdBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUE7UUFDN0MsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxHQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUMsQ0FBQyxFQUFFLEVBQUM7WUFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBRWhDLElBQUksU0FBUyxHQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtZQUM3QyxJQUFJLEdBQUcsR0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUE7WUFDdEMsSUFBSSxHQUFHLEdBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFBO1lBRXRDLElBQUksSUFBSSxHQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFDLEdBQUcsR0FBQyxHQUFHLEdBQUMsR0FBRyxDQUFDLENBQUE7WUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVsQixJQUFHLElBQUksR0FBQyxFQUFFLEVBQUM7Z0JBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFbkIsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBQyxHQUFHLENBQUE7YUFDaEM7U0FDSjtJQUdMLENBQUM7SUFDRCxRQUFRO0lBQ1IsdUJBQVEsR0FBUjs7UUFDSSxNQUFBLElBQUksQ0FBQyxJQUFJLDBDQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUUsVUFBVSxLQUFVOztZQUM1QyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFBO1lBQ3pCLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQTtZQUNuQyxJQUFJLGFBQWEsU0FBRyxJQUFJLENBQUMsSUFBSSwwQ0FBRSxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQTtZQUU5RCxJQUFJLFFBQVEsU0FBRyxJQUFJLENBQUMsSUFBSSwwQ0FBRSxRQUFRLENBQUM7WUFDbkMsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3RDLElBQUksZUFBZSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDbkQsSUFBSSxlQUFlLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxFQUFFO3dCQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFBO3dCQUNoRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTt3QkFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDbkIsSUFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDM0QsSUFBSSxhQUFhLEVBQUU7NEJBQ2YsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO3lCQUN6Qjt3QkFDRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUE7cUJBRXRCO2lCQUNKO2dCQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLGFBQWEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUMvRDtRQUVMLENBQUMsRUFBRSxJQUFJLEVBQUU7UUFDVCxNQUFBLElBQUksQ0FBQyxJQUFJLDBDQUFFLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBVSxLQUFVOztZQUMzQyw0QkFBNEI7WUFDNUIsSUFBSSxRQUFRLFNBQUcsSUFBSSxDQUFDLElBQUksMENBQUUsUUFBUSxDQUFDO1lBQ25DLElBQUksQ0FBQyxRQUFRO2dCQUFFLE9BQU07WUFDckIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3RDLElBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzNELElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxPQUFPLEVBQUU7b0JBQ3hDLHFDQUFxQztvQkFDckMsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUNoQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFBO29CQUM1RCxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFBO29CQUM1RCxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtvQkFFN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNoRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUE7b0JBR25CLCtEQUErRDtvQkFDL0Qsd0NBQXdDO2lCQUMzQzthQUNKO1FBRUwsQ0FBQyxFQUFFLElBQUksRUFBRTtRQUNULE1BQUEsSUFBSSxDQUFDLElBQUksMENBQUUsRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLEtBQVU7O1lBQzFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDeEIsSUFBSSxRQUFRLFNBQUcsSUFBSSxDQUFDLElBQUksMENBQUUsUUFBUSxDQUFDO1lBQ25DLElBQUksQ0FBQyxRQUFRO2dCQUFFLE9BQU07WUFDckIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3RDLElBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzNELElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxPQUFPLEVBQUU7b0JBQ3hDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDekI7YUFDSjtRQUNMLENBQUMsRUFBRSxJQUFJLEVBQUU7UUFDVCxNQUFBLElBQUksQ0FBQyxJQUFJLDBDQUFFLEVBQUUsQ0FBQyxhQUFhLEVBQUU7WUFDekIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDbEMsSUFBSSxDQUFDLFFBQVE7Z0JBQUUsT0FBTTtZQUNyQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDdEMsSUFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDM0QsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLE9BQU8sRUFBRTtvQkFDeEMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUN6QjthQUNKO1FBQ0wsQ0FBQyxFQUFFLElBQUksRUFBRTtJQUNiLENBQUM7SUFFRCxZQUFZO0lBQ1osNEJBQWEsR0FBYjtRQUNJLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEIsSUFBSSxtQkFBbUIsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ2xFLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3ZDLElBQUksYUFBYSxHQUFHLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNuRSxJQUFJLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ1IsY0FBYyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDcEM7aUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNmLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQzthQUNuQztZQUNELG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQTtZQUUvQyxJQUFJLGFBQWEsRUFBRTtnQkFDZixhQUFhLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO2FBRXJDO1NBQ0o7SUFFTCxDQUFDO0lBQ0QsT0FBTztJQUNQLHlCQUFVLEdBQVY7UUFDSSxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSTtZQUFFLE9BQU87UUFDdEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN6QixJQUFJLFlBQVksR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFBLFFBQVE7WUFDN0QsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1NBQ3hDO0lBQ0wsQ0FBQztJQUNELFVBQVU7SUFDVix1QkFBUSxHQUFSO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUk7WUFBRSxPQUFPO1FBQ25DLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDekIsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxRQUFRO1lBQ3hELFVBQVUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN0QyxVQUFVLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFBLGFBQWE7WUFDcEMsSUFBSSxRQUFRLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLGtCQUFrQjtZQUNsRSxJQUFJLFFBQVEsRUFBRTtnQkFDVixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN0QixRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO2FBQ25CO1NBQ0o7SUFDTCxDQUFDO0lBQ0Qsb0JBQUssR0FBTDtJQUVBLENBQUM7SUFyTEQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzt1Q0FDSTtJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzhDQUNXO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7MkNBQ1E7SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztvREFDaUI7SUFFckM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsyQ0FDUTtJQUUxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhDQUNXO0lBRzdCO1FBREMsUUFBUTtzQ0FDYTtJQWhCTCxJQUFJO1FBRHhCLE9BQU87T0FDYSxJQUFJLENBMkx4QjtJQUFELFdBQUM7Q0EzTEQsQUEyTEMsQ0EzTGlDLEVBQUUsQ0FBQyxTQUFTLEdBMkw3QztrQkEzTG9CLElBQUkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdhbWUgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGxhYmVsOiBjYy5MYWJlbCA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcbiAgICBwcmVfYmxvY2tfYmc6IGNjLlByZWZhYiA9IG51bGw7Ly/pooTliLbotYTmupBcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxuICAgIHByZV9ibG9jazogY2MuUHJlZmFiID0gbnVsbDsvL+mihOWItui1hOa6kFxuICAgIEBwcm9wZXJ0eShjYy5QcmVmYWIpXG4gICAgcHJlX2xheW91dF9ibG9ja18xOiBjYy5QcmVmYWIgPSBudWxsOy8v6aKE5Yi26LWE5rqQXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgbGF5b3V0X2JnOiBjYy5Ob2RlID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBsYXlvdXRfYmxvY2s6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5XG4gICAgdGV4dDogc3RyaW5nID0gJ2hlbGxvJ1xuICAgIGJnXzM6IGFueVxuICAgIHBvc190b3VjaF9zdGFydDogYW55XG5cbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcblxuICAgIG9uTG9hZCgpIHtcbiAgICAgICAgdGhpcy5iZ18zID0gdGhpcy5ub2RlPy5nZXRDaGlsZEJ5TmFtZSgnYmcnKT8uZ2V0Q2hpbGRCeU5hbWUoXCJiZ18zXCIpO1xuICAgICAgICB0aGlzLmFkZEJsb2NrQmcoKTtcbiAgICAgICAgdGhpcy5hZGRCbG9jaygpXG4gICAgICAgIHRoaXMuYWRkQmxvY2tUb3VjaCgpXG4gICAgICAgIHRoaXMuc2V0VG91Y2goKVxuICAgIH1cblxuICAgIC8v5Yik5pat5Y+v54K55Ye75YWD57Sg5Z2X5ZKM5q2j5bi45Y6f55Sf5Z2X5LmL6Ze055qE6Led56a7XG4gICAgcGRKdUxpX2Jsb2NrKCkge1xuICAgICAgICBsZXQgcG9zX3RvdWNoQmxvY2s9Y2MudjIoMCwwKVxuICAgICAgICBsZXQgY2hpbGRyZW5fdG91Y2ggPSB0aGlzLmJnXzM/LmNoaWxkcmVuO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNoaWxkcmVuX3RvdWNoLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQganNfYmxvY2tUb3VjaCA9IGNoaWxkcmVuX3RvdWNoW2ldLmdldENvbXBvbmVudChcImJsb2NrVG91Y2hcIik7XG4gICAgICAgICAgICBpZiAoanNfYmxvY2tUb3VjaCAmJiBqc19ibG9ja1RvdWNoLmlzVG91Y2gpIHtcbiAgICAgICAgICAgICAgICBsZXQgcG9zX3RvdWNoV29ybGQ9anNfYmxvY2tUb3VjaC5nZXRQb3NXb3JkKClcbiAgICAgICAgICAgICAgICBwb3NfdG91Y2hCbG9jaz10aGlzLmxheW91dF9ibG9jay5jb252ZXJ0VG9Ob2RlU3BhY2VBUihwb3NfdG91Y2hXb3JsZClcbiAgICAgICAgICAgICAgICBcbjBcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgICAgY29uc29sZS5sb2coJ3hhOicrcG9zX3RvdWNoQmxvY2sueSk7XG4gICAgICAgICAgXG4gICAgICAgIGxldCBjaGlsZHJlbl9ibG9jaz10aGlzLmxheW91dF9ibG9jay5jaGlsZHJlblxuICAgICAgICBmb3IobGV0IGk9MDtpPGNoaWxkcmVuX2Jsb2NrLmxlbmd0aDtpKyspe1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCIxMTExMTExMTExMTExMTExXCIpO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBsZXQgcG9zX2Jsb2NrPWNoaWxkcmVuX2Jsb2NrW2ldLmdldFBvc2l0aW9uKClcbiAgICAgICAgICAgIGxldCBmX3g9cG9zX2Jsb2NrLnggLSBwb3NfdG91Y2hCbG9jay54XG4gICAgICAgICAgICBsZXQgZl95PXBvc19ibG9jay55IC0gcG9zX3RvdWNoQmxvY2sueVxuXG4gICAgICAgICAgICBsZXQgZl9qbD1NYXRoLnNxcnQoZl94KmZfeCtmX3kqZl95KVxuICAgICAgICAgICAgY29uc29sZS5sb2coZl9qbCk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmKGZfamw8NDApe1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwi5oiR5p2l5LqGXCIpO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGNoaWxkcmVuX2Jsb2NrW2ldLm9wYWNpdHk9MTAwXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuXG4gICAgfVxuICAgIC8v5bGP5bmV6Kem5pG45LqL5Lu2XG4gICAgc2V0VG91Y2goKSB7XG4gICAgICAgIHRoaXMubm9kZT8ub24oJ3RvdWNoc3RhcnQnLCBmdW5jdGlvbiAoZXZlbnQ6IGFueSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJ0b3VjaHN0YXJ0XCIpXG4gICAgICAgICAgICBsZXQgcG9zX3N0YXJ0ID0gZXZlbnQuZ2V0TG9jYXRpb24oKVxuICAgICAgICAgICAgbGV0IHBvc19zdGFydF9iZzMgPSB0aGlzLmJnXzM/LmNvbnZlcnRUb05vZGVTcGFjZUFSKHBvc19zdGFydClcblxuICAgICAgICAgICAgbGV0IGNoaWxkcmVuID0gdGhpcy5iZ18zPy5jaGlsZHJlbjtcbiAgICAgICAgICAgIGlmIChjaGlsZHJlbikge1xuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJlY3RfYmxvY2tUb3VjaCA9IGNoaWxkcmVuW2ldLmdldEJvdW5kaW5nQm94KCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZWN0X2Jsb2NrVG91Y2guY29udGFpbnMocG9zX3N0YXJ0X2JnMykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucG9zX3RvdWNoX3N0YXJ0ID0gY2MudjIoY2hpbGRyZW5baV0ueCwgY2hpbGRyZW5baV0ueSArIDE1MClcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoaWxkcmVuW2ldLnNldFBvc2l0aW9uKHRoaXMucG9zX3RvdWNoX3N0YXJ0KVxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLngrnkuK3kuoZcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQganNfYmxvY2tUb3VjaCA9IGNoaWxkcmVuW2ldLmdldENvbXBvbmVudChcImJsb2NrVG91Y2hcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoanNfYmxvY2tUb3VjaCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGpzX2Jsb2NrVG91Y2gudG91Y2goKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucGRKdUxpX2Jsb2NrKClcblxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCd4OicgKyBwb3Nfc3RhcnRfYmczLngsIFwieTpcIiArIHBvc19zdGFydF9iZzMueSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfSwgdGhpcyk7XG4gICAgICAgIHRoaXMubm9kZT8ub24oJ3RvdWNobW92ZScsIGZ1bmN0aW9uIChldmVudDogYW55KSB7XG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhcInRvdWNobW92ZVwiKTtcbiAgICAgICAgICAgIGxldCBjaGlsZHJlbiA9IHRoaXMuYmdfMz8uY2hpbGRyZW47XG4gICAgICAgICAgICBpZiAoIWNoaWxkcmVuKSByZXR1cm5cbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBsZXQganNfYmxvY2tUb3VjaCA9IGNoaWxkcmVuW2ldLmdldENvbXBvbmVudChcImJsb2NrVG91Y2hcIik7XG4gICAgICAgICAgICAgICAgaWYgKGpzX2Jsb2NrVG91Y2ggJiYganNfYmxvY2tUb3VjaC5pc1RvdWNoKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGxldCBwb3NfbW92ZSA9IGV2ZW50LmdldExvY2F0aW9uKClcbiAgICAgICAgICAgICAgICAgICAgbGV0IHBvc19tb3ZlID0gZXZlbnQuZ2V0RGVsdGEoKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wb3NfdG91Y2hfc3RhcnQueCA9IHRoaXMucG9zX3RvdWNoX3N0YXJ0LnggKyBwb3NfbW92ZS54XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucG9zX3RvdWNoX3N0YXJ0LnkgPSB0aGlzLnBvc190b3VjaF9zdGFydC55ICsgcG9zX21vdmUueVxuICAgICAgICAgICAgICAgICAgICBjaGlsZHJlbltpXS5zZXRQb3NpdGlvbih0aGlzLnBvc190b3VjaF9zdGFydClcblxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygneDonLCBwb3NfbW92ZS54LCBcInk6XCIsIHBvc19tb3ZlLnkpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnBkSnVMaV9ibG9jaygpXG5cblxuICAgICAgICAgICAgICAgICAgICAvLyBsZXQgcG9zX21vdmVfYmczID0gdGhpcy5iZ18zPy5jb252ZXJ0VG9Ob2RlU3BhY2VBUihwb3NfbW92ZSlcbiAgICAgICAgICAgICAgICAgICAgLy8gY2hpbGRyZW5baV0uc2V0UG9zaXRpb24ocG9zX21vdmVfYmczKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9LCB0aGlzKTtcbiAgICAgICAgdGhpcy5ub2RlPy5vbigndG91Y2hlbmQnLCBmdW5jdGlvbiAoZXZlbnQ6IGFueSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJ0b3VjaGVuZFwiKTtcbiAgICAgICAgICAgIGxldCBjaGlsZHJlbiA9IHRoaXMuYmdfMz8uY2hpbGRyZW47XG4gICAgICAgICAgICBpZiAoIWNoaWxkcmVuKSByZXR1cm5cbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBsZXQganNfYmxvY2tUb3VjaCA9IGNoaWxkcmVuW2ldLmdldENvbXBvbmVudChcImJsb2NrVG91Y2hcIik7XG4gICAgICAgICAgICAgICAgaWYgKGpzX2Jsb2NrVG91Y2ggJiYganNfYmxvY2tUb3VjaC5pc1RvdWNoKSB7XG4gICAgICAgICAgICAgICAgICAgIGpzX2Jsb2NrVG91Y2guZnVXZWkoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHRoaXMpO1xuICAgICAgICB0aGlzLm5vZGU/Lm9uKCd0b3VjaGNhbmNlbCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGxldCBjaGlsZHJlbiA9IHRoaXMuYmdfMy5jaGlsZHJlbjtcbiAgICAgICAgICAgIGlmICghY2hpbGRyZW4pIHJldHVyblxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGxldCBqc19ibG9ja1RvdWNoID0gY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KFwiYmxvY2tUb3VjaFwiKTtcbiAgICAgICAgICAgICAgICBpZiAoanNfYmxvY2tUb3VjaCAmJiBqc19ibG9ja1RvdWNoLmlzVG91Y2gpIHtcbiAgICAgICAgICAgICAgICAgICAganNfYmxvY2tUb3VjaC5mdVdlaSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgdGhpcyk7XG4gICAgfVxuXG4gICAgLy/mt7vliqDlj6/ku6Xngrnlh7vnmoTlhYPntKDlnZdcbiAgICBhZGRCbG9ja1RvdWNoKCkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDM7IGkrKykge1xuICAgICAgICAgICAgbGV0IG5vZGVfbGF5b3V0X2Jsb2NrXzEgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLnByZV9sYXlvdXRfYmxvY2tfMSk7XG4gICAgICAgICAgICBub2RlX2xheW91dF9ibG9ja18xLnBhcmVudCA9IHRoaXMuYmdfMztcbiAgICAgICAgICAgIGxldCBqc19ibG9ja1RvdWNoID0gbm9kZV9sYXlvdXRfYmxvY2tfMS5nZXRDb21wb25lbnQoJ2Jsb2NrVG91Y2gnKTtcbiAgICAgICAgICAgIGxldCBwb3NfQmxvY2t0b3VjaCA9IGNjLnYyKDAsIDE0KTtcbiAgICAgICAgICAgIGlmIChpID09IDApIHtcbiAgICAgICAgICAgICAgICBwb3NfQmxvY2t0b3VjaCA9IGNjLnYyKC0xOTMsIDE0KTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoaSA9PSAyKSB7XG4gICAgICAgICAgICAgICAgcG9zX0Jsb2NrdG91Y2ggPSBjYy52MigxOTQsIDE0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG5vZGVfbGF5b3V0X2Jsb2NrXzEuc2V0UG9zaXRpb24ocG9zX0Jsb2NrdG91Y2gpXG5cbiAgICAgICAgICAgIGlmIChqc19ibG9ja1RvdWNoKSB7XG4gICAgICAgICAgICAgICAganNfYmxvY2tUb3VjaC5pbml0KHBvc19CbG9ja3RvdWNoKVxuXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgIH1cbiAgICAvL+a3u+WKoOiDjOaZr+Wdl1xuICAgIGFkZEJsb2NrQmcoKSB7XG4gICAgICAgIGlmICh0aGlzLnByZV9ibG9ja19iZyA9PSBudWxsKSByZXR1cm47XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgNjQ7IGkrKykge1xuICAgICAgICAgICAgbGV0IG5vZGVfYmxvY2tCZyA9IGNjLmluc3RhbnRpYXRlKHRoaXMucHJlX2Jsb2NrX2JnKTsvL+mihOWItuS9k+WunuS+i+WMllxuICAgICAgICAgICAgbm9kZV9ibG9ja0JnLnBhcmVudCA9IHRoaXMubGF5b3V0X2JnO1xuICAgICAgICB9XG4gICAgfVxuICAgIC8v5re75Yqg5omA5pyJ55qE5YWD57Sg5Z2XXG4gICAgYWRkQmxvY2soKSB7XG4gICAgICAgIGlmICh0aGlzLnByZV9ibG9jayA9PSBudWxsKSByZXR1cm47XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgNjQ7IGkrKykge1xuICAgICAgICAgICAgbGV0IG5vZGVfYmxvY2sgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLnByZV9ibG9jayk7Ly/pooTliLbkvZPlrp7kvovljJZcbiAgICAgICAgICAgIG5vZGVfYmxvY2sucGFyZW50ID0gdGhpcy5sYXlvdXRfYmxvY2s7XG4gICAgICAgICAgICBub2RlX2Jsb2NrLm9wYWNpdHkgPSAwOy8v5omA5pyJ6Imy5Z2X6buY6K6k6YCP5piO5bqm5Li6MFxuICAgICAgICAgICAgbGV0IGpzX2Jsb2NrID0gbm9kZV9ibG9jay5nZXRDb21wb25lbnQoJ2Jsb2NrJyk7Ly/ku45ub2RlX2Jsb2Nr5Lit6I635Y+W5LqL5Lu2XG4gICAgICAgICAgICBpZiAoanNfYmxvY2spIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIuaIkei1sOWIsOS6hui/memHjFwiKTtcbiAgICAgICAgICAgICAgICBqc19ibG9jay5pbml0KDQpXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgc3RhcnQoKSB7XG5cbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxufVxuIl19
//------QC-SOURCE-SPLIT------
