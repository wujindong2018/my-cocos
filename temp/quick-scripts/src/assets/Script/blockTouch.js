"use strict";
cc._RF.push(module, '603fanDJX5Mt54Sxi47Vp/Q', 'blockTouch');
// Script/blockTouch.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BlockTouch = /** @class */ (function (_super) {
    __extends(BlockTouch, _super);
    function BlockTouch() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        //全局属性及类型
        _this.spf_block = [];
        _this.text = 'hello';
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    BlockTouch.prototype.onLoad = function () {
    };
    BlockTouch.prototype.init = function (_possStart) {
        this.pos_start = _possStart; //起始位置
        this.isTouch = false; //是否点中
        //随机改变元素块颜色
        this.i_blockType = Math.floor(Math.random() * this.spf_block.length);
        var children = this.node.children;
        for (var i = 0; i < children.length; i++) {
            if (children[i].name == '1') {
                children[i].getComponent(cc.Sprite).spriteFrame = this.spf_block[this.i_blockType];
            }
        }
    };
    //获取世界坐标
    BlockTouch.prototype.getPosWord = function () {
        var pos_world = cc.v2(0, 0);
        var children = this.node.children;
        for (var i = 0; i < children.length; i++) {
            if (children[i].name == '1') {
                pos_world = this.node.convertToNodeSpaceAR(children[i].getPosition());
                break;
                // children[i].getComponent(cc.Sprite).spriteFrame = this.spf_block[this.i_blockType]
            }
        }
        return pos_world;
    };
    //复位回到起始位置
    BlockTouch.prototype.fuWei = function () {
        this.node.scale = 0.8;
        this.isTouch = false; //是否点中
        this.node.setPosition(this.pos_start);
    };
    //点中了放大
    BlockTouch.prototype.touch = function () {
        this.isTouch = true;
        this.node.scale = 1;
    };
    BlockTouch.prototype.start = function () {
    };
    __decorate([
        property(cc.Label)
    ], BlockTouch.prototype, "label", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], BlockTouch.prototype, "spf_block", void 0);
    __decorate([
        property
    ], BlockTouch.prototype, "text", void 0);
    BlockTouch = __decorate([
        ccclass
    ], BlockTouch);
    return BlockTouch;
}(cc.Component));
exports.default = BlockTouch;

cc._RF.pop();