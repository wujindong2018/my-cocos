"use strict";
cc._RF.push(module, '373494UlHRFD5yXVQr/OoZa', 'Game');
// Script/Game.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Game = /** @class */ (function (_super) {
    __extends(Game, _super);
    function Game() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        _this.pre_block_bg = null; //预制资源
        _this.pre_block = null; //预制资源
        _this.pre_layout_block_1 = null; //预制资源
        _this.layout_bg = null;
        _this.layout_block = null;
        _this.text = 'hello';
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    Game.prototype.onLoad = function () {
        var _a, _b;
        this.bg_3 = (_b = (_a = this.node) === null || _a === void 0 ? void 0 : _a.getChildByName('bg')) === null || _b === void 0 ? void 0 : _b.getChildByName("bg_3");
        this.addBlockBg();
        this.addBlock();
        this.addBlockTouch();
        this.setTouch();
    };
    //判断可点击元素块和正常原生块之间的距离
    Game.prototype.pdJuLi_block = function () {
        var _a;
        var pos_touchBlock = cc.v2(0, 0);
        var children_touch = (_a = this.bg_3) === null || _a === void 0 ? void 0 : _a.children;
        for (var i = 0; i < children_touch.length; i++) {
            var js_blockTouch = children_touch[i].getComponent("blockTouch");
            if (js_blockTouch && js_blockTouch.isTouch) {
                var pos_touchWorld = js_blockTouch.getPosWord();
                pos_touchBlock = this.layout_block.convertToNodeSpaceAR(pos_touchWorld);
                0;
            }
        }
        console.log('xa:' + pos_touchBlock.y);
        var children_block = this.layout_block.children;
        for (var i = 0; i < children_block.length; i++) {
            console.log("1111111111111111");
            var pos_block = children_block[i].getPosition();
            var f_x = pos_block.x - pos_touchBlock.x;
            var f_y = pos_block.y - pos_touchBlock.y;
            var f_jl = Math.sqrt(f_x * f_x + f_y * f_y);
            console.log(f_jl);
            if (f_jl < 40) {
                console.log("我来了");
                children_block[i].opacity = 100;
            }
        }
    };
    //屏幕触摸事件
    Game.prototype.setTouch = function () {
        var _a, _b, _c, _d;
        (_a = this.node) === null || _a === void 0 ? void 0 : _a.on('touchstart', function (event) {
            var _a, _b;
            console.log("touchstart");
            var pos_start = event.getLocation();
            var pos_start_bg3 = (_a = this.bg_3) === null || _a === void 0 ? void 0 : _a.convertToNodeSpaceAR(pos_start);
            var children = (_b = this.bg_3) === null || _b === void 0 ? void 0 : _b.children;
            if (children) {
                for (var i = 0; i < children.length; i++) {
                    var rect_blockTouch = children[i].getBoundingBox();
                    if (rect_blockTouch.contains(pos_start_bg3)) {
                        this.pos_touch_start = cc.v2(children[i].x, children[i].y + 150);
                        children[i].setPosition(this.pos_touch_start);
                        console.log("点中了");
                        var js_blockTouch = children[i].getComponent("blockTouch");
                        if (js_blockTouch) {
                            js_blockTouch.touch();
                        }
                        this.pdJuLi_block();
                    }
                }
                console.log('x:' + pos_start_bg3.x, "y:" + pos_start_bg3.y);
            }
        }, this);
        (_b = this.node) === null || _b === void 0 ? void 0 : _b.on('touchmove', function (event) {
            var _a;
            // console.log("touchmove");
            var children = (_a = this.bg_3) === null || _a === void 0 ? void 0 : _a.children;
            if (!children)
                return;
            for (var i = 0; i < children.length; i++) {
                var js_blockTouch = children[i].getComponent("blockTouch");
                if (js_blockTouch && js_blockTouch.isTouch) {
                    // let pos_move = event.getLocation()
                    var pos_move = event.getDelta();
                    this.pos_touch_start.x = this.pos_touch_start.x + pos_move.x;
                    this.pos_touch_start.y = this.pos_touch_start.y + pos_move.y;
                    children[i].setPosition(this.pos_touch_start);
                    console.log('x:', pos_move.x, "y:", pos_move.y);
                    this.pdJuLi_block();
                    // let pos_move_bg3 = this.bg_3?.convertToNodeSpaceAR(pos_move)
                    // children[i].setPosition(pos_move_bg3)
                }
            }
        }, this);
        (_c = this.node) === null || _c === void 0 ? void 0 : _c.on('touchend', function (event) {
            var _a;
            console.log("touchend");
            var children = (_a = this.bg_3) === null || _a === void 0 ? void 0 : _a.children;
            if (!children)
                return;
            for (var i = 0; i < children.length; i++) {
                var js_blockTouch = children[i].getComponent("blockTouch");
                if (js_blockTouch && js_blockTouch.isTouch) {
                    js_blockTouch.fuWei();
                }
            }
        }, this);
        (_d = this.node) === null || _d === void 0 ? void 0 : _d.on('touchcancel', function () {
            var children = this.bg_3.children;
            if (!children)
                return;
            for (var i = 0; i < children.length; i++) {
                var js_blockTouch = children[i].getComponent("blockTouch");
                if (js_blockTouch && js_blockTouch.isTouch) {
                    js_blockTouch.fuWei();
                }
            }
        }, this);
    };
    //添加可以点击的元素块
    Game.prototype.addBlockTouch = function () {
        for (var i = 0; i < 3; i++) {
            var node_layout_block_1 = cc.instantiate(this.pre_layout_block_1);
            node_layout_block_1.parent = this.bg_3;
            var js_blockTouch = node_layout_block_1.getComponent('blockTouch');
            var pos_Blocktouch = cc.v2(0, 14);
            if (i == 0) {
                pos_Blocktouch = cc.v2(-193, 14);
            }
            else if (i == 2) {
                pos_Blocktouch = cc.v2(194, 14);
            }
            node_layout_block_1.setPosition(pos_Blocktouch);
            if (js_blockTouch) {
                js_blockTouch.init(pos_Blocktouch);
            }
        }
    };
    //添加背景块
    Game.prototype.addBlockBg = function () {
        if (this.pre_block_bg == null)
            return;
        for (var i = 0; i < 64; i++) {
            var node_blockBg = cc.instantiate(this.pre_block_bg); //预制体实例化
            node_blockBg.parent = this.layout_bg;
        }
    };
    //添加所有的元素块
    Game.prototype.addBlock = function () {
        if (this.pre_block == null)
            return;
        for (var i = 0; i < 64; i++) {
            var node_block = cc.instantiate(this.pre_block); //预制体实例化
            node_block.parent = this.layout_block;
            node_block.opacity = 0; //所有色块默认透明度为0
            var js_block = node_block.getComponent('block'); //从node_block中获取事件
            if (js_block) {
                console.log("我走到了这里");
                js_block.init(4);
            }
        }
    };
    Game.prototype.start = function () {
    };
    __decorate([
        property(cc.Label)
    ], Game.prototype, "label", void 0);
    __decorate([
        property(cc.Prefab)
    ], Game.prototype, "pre_block_bg", void 0);
    __decorate([
        property(cc.Prefab)
    ], Game.prototype, "pre_block", void 0);
    __decorate([
        property(cc.Prefab)
    ], Game.prototype, "pre_layout_block_1", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "layout_bg", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "layout_block", void 0);
    __decorate([
        property
    ], Game.prototype, "text", void 0);
    Game = __decorate([
        ccclass
    ], Game);
    return Game;
}(cc.Component));
exports.default = Game;

cc._RF.pop();