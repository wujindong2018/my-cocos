// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Game extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;
    @property(cc.Prefab)
    pre_block_bg: cc.Prefab = null;//预制资源
    @property(cc.Prefab)
    pre_block: cc.Prefab = null;//预制资源
    @property(cc.Prefab)
    pre_layout_block_1: cc.Prefab = null;//预制资源
    @property(cc.Node)
    layout_bg: cc.Node = null;
    @property(cc.Node)
    layout_block: cc.Node = null;

    @property
    text: string = 'hello'
    bg_3: any
    pos_touch_start: any

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.bg_3 = this.node?.getChildByName('bg')?.getChildByName("bg_3");
        this.addBlockBg();
        this.addBlock()
        this.addBlockTouch()
        this.setTouch()
    }

    //判断可点击元素块和正常原生块之间的距离
    pdJuLi_block() {
        let pos_touchBlock=cc.v2(0,0)
        let children_touch = this.bg_3?.children;
        for (let i = 0; i < children_touch.length; i++) {
            let js_blockTouch = children_touch[i].getComponent("blockTouch");
            if (js_blockTouch && js_blockTouch.isTouch) {
                let pos_touchWorld=js_blockTouch.getPosWord()
                pos_touchBlock=this.layout_block.convertToNodeSpaceAR(pos_touchWorld)
                
0
            }
        }

          console.log('xa:'+pos_touchBlock.y);
          
        let children_block=this.layout_block.children
        for(let i=0;i<children_block.length;i++){
            console.log("1111111111111111");
            
            let pos_block=children_block[i].getPosition()
            let f_x=pos_block.x - pos_touchBlock.x
            let f_y=pos_block.y - pos_touchBlock.y

            let f_jl=Math.sqrt(f_x*f_x+f_y*f_y)
            console.log(f_jl);
            
            if(f_jl<40){
                console.log("我来了");
                
                children_block[i].opacity=100
            }
        }


    }
    //屏幕触摸事件
    setTouch() {
        this.node?.on('touchstart', function (event: any) {
            console.log("touchstart")
            let pos_start = event.getLocation()
            let pos_start_bg3 = this.bg_3?.convertToNodeSpaceAR(pos_start)

            let children = this.bg_3?.children;
            if (children) {
                for (let i = 0; i < children.length; i++) {
                    let rect_blockTouch = children[i].getBoundingBox();
                    if (rect_blockTouch.contains(pos_start_bg3)) {
                        this.pos_touch_start = cc.v2(children[i].x, children[i].y + 150)
                        children[i].setPosition(this.pos_touch_start)
                        console.log("点中了");
                        let js_blockTouch = children[i].getComponent("blockTouch");
                        if (js_blockTouch) {
                            js_blockTouch.touch();
                        }
                        this.pdJuLi_block()

                    }
                }
                console.log('x:' + pos_start_bg3.x, "y:" + pos_start_bg3.y);
            }

        }, this);
        this.node?.on('touchmove', function (event: any) {
            // console.log("touchmove");
            let children = this.bg_3?.children;
            if (!children) return
            for (let i = 0; i < children.length; i++) {
                let js_blockTouch = children[i].getComponent("blockTouch");
                if (js_blockTouch && js_blockTouch.isTouch) {
                    // let pos_move = event.getLocation()
                    let pos_move = event.getDelta();
                    this.pos_touch_start.x = this.pos_touch_start.x + pos_move.x
                    this.pos_touch_start.y = this.pos_touch_start.y + pos_move.y
                    children[i].setPosition(this.pos_touch_start)

                    console.log('x:', pos_move.x, "y:", pos_move.y);
                    this.pdJuLi_block()


                    // let pos_move_bg3 = this.bg_3?.convertToNodeSpaceAR(pos_move)
                    // children[i].setPosition(pos_move_bg3)
                }
            }

        }, this);
        this.node?.on('touchend', function (event: any) {
            console.log("touchend");
            let children = this.bg_3?.children;
            if (!children) return
            for (let i = 0; i < children.length; i++) {
                let js_blockTouch = children[i].getComponent("blockTouch");
                if (js_blockTouch && js_blockTouch.isTouch) {
                    js_blockTouch.fuWei();
                }
            }
        }, this);
        this.node?.on('touchcancel', function () {
            let children = this.bg_3.children;
            if (!children) return
            for (let i = 0; i < children.length; i++) {
                let js_blockTouch = children[i].getComponent("blockTouch");
                if (js_blockTouch && js_blockTouch.isTouch) {
                    js_blockTouch.fuWei();
                }
            }
        }, this);
    }

    //添加可以点击的元素块
    addBlockTouch() {
        for (let i = 0; i < 3; i++) {
            let node_layout_block_1 = cc.instantiate(this.pre_layout_block_1);
            node_layout_block_1.parent = this.bg_3;
            let js_blockTouch = node_layout_block_1.getComponent('blockTouch');
            let pos_Blocktouch = cc.v2(0, 14);
            if (i == 0) {
                pos_Blocktouch = cc.v2(-193, 14);
            } else if (i == 2) {
                pos_Blocktouch = cc.v2(194, 14);
            }
            node_layout_block_1.setPosition(pos_Blocktouch)

            if (js_blockTouch) {
                js_blockTouch.init(pos_Blocktouch)

            }
        }

    }
    //添加背景块
    addBlockBg() {
        if (this.pre_block_bg == null) return;
        for (let i = 0; i < 64; i++) {
            let node_blockBg = cc.instantiate(this.pre_block_bg);//预制体实例化
            node_blockBg.parent = this.layout_bg;
        }
    }
    //添加所有的元素块
    addBlock() {
        if (this.pre_block == null) return;
        for (let i = 0; i < 64; i++) {
            let node_block = cc.instantiate(this.pre_block);//预制体实例化
            node_block.parent = this.layout_block;
            node_block.opacity = 0;//所有色块默认透明度为0
            let js_block = node_block.getComponent('block');//从node_block中获取事件
            if (js_block) {
                console.log("我走到了这里");
                js_block.init(4)
            }
        }
    }
    start() {

    }

    // update (dt) {}
}
