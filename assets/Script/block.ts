// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Block extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    i_type:any

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }

    init(_iType: void){
        this.i_type=_iType;
        this.showBlock();
    }
    //显示哪个色块
    showBlock() {
        let children = this.node.children;
        for (let i = 0; i < children.length; i++) {
            if (children[i].name == this.i_type) {
                children[i].active = true;//显示色块
            } else {
                children[i].active = false;//隐藏色块
            }
        }
    }

    // update (dt) {}
}
