// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class BlockTouch extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property([cc.SpriteFrame])
    //全局属性及类型
    spf_block: [cc.SpriteFrame] = <any>[];

    @property
    text: string = 'hello';
    i_blockType: number;
    isTouch: boolean
    pos_start: any;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {


    }

    init(_possStart: any){
        this.pos_start = _possStart;//起始位置
        this.isTouch = false;//是否点中
        //随机改变元素块颜色
        this.i_blockType = Math.floor(Math.random() * this.spf_block.length);
        let children = this.node.children;
        for (let i = 0; i < children.length; i++) {
            if (children[i].name == '1') {
                children[i].getComponent(cc.Sprite).spriteFrame = this.spf_block[this.i_blockType]
            }
        }

    }

    //获取世界坐标
    getPosWord(){
        let pos_world=cc.v2(0,0)
        let children = this.node.children;
        for (let i = 0; i < children.length; i++) {
            if (children[i].name == '1') {
                pos_world=this.node.convertToNodeSpaceAR(children[i].getPosition())
                break
                // children[i].getComponent(cc.Sprite).spriteFrame = this.spf_block[this.i_blockType]
            }
        }
        return pos_world;
    }

    //复位回到起始位置
    fuWei() {
        this.node.scale=0.8;
        this.isTouch = false;//是否点中
        this.node.setPosition(this.pos_start)

    }

    //点中了放大
    touch(){
        this.isTouch=true;
        this.node.scale=1

    }

    start() {

    }

    // update (dt) {}
}
